package pl.cyfrowypolsat.zbh.sautcap;

import android.content.Intent;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SAUTCAP-MACT";
    private static final int REQUEST_CODE = 1;
    private MediaProjectionManager mMediaProjectionManager;
    private Recorder mRecorder;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(this);
        mMediaProjectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        MediaProjection mediaProjection = mMediaProjectionManager.getMediaProjection(resultCode, data);
        if (mediaProjection == null) {
            return;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        final int width = displayMetrics.widthPixels;
        final int height = displayMetrics.heightPixels;
        final int dpi = displayMetrics.densityDpi;
        File file = new File(Environment.getExternalStorageDirectory(),
                "sautcap_screen.mp4");
        final int bitrate = 1500000;

        mRecorder = new Recorder(width, height, bitrate, dpi,
                mediaProjection, file.getAbsolutePath());
        mRecorder.start();
        mButton.setText(R.string.button_cap_stop);

        moveTaskToBack(true);
    }

    @Override
    public void onClick(View v) {
        if (mRecorder == null) {
            Intent captureIntent = mMediaProjectionManager.createScreenCaptureIntent();
            startActivityForResult(captureIntent, REQUEST_CODE);
        } else {
            mRecorder.quit();
            mButton.setText(R.string.button_cap_restart);
            try {
                mRecorder.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mRecorder = null;
        }
    }

    @Override
    public void onDestroy() {
        try {
            mRecorder.quit();
            mRecorder.join();
            mRecorder.release();
        } catch (Exception e) {
        } finally {
            super.onDestroy();
        }
    }
}